#!/bin/bash
set -exu

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

trap "cd \"${script_dir}\"" EXIT

install_or_update_vim_module() {
    repourl=https://github.com/$1
    repo=$(basename "${repourl}" .git)
    cd ~/.vim/bundle
    if [ ! -d  "${repo}" ]
    then
        git clone "${repourl}"
    else
        cd "${repo}"
        git pull
    fi
}

create_link() {
    src=$1
    dst=$2
    if [ ! -e "${dst}" ]
    then
        ln -s "${src}" "${dst}"
    fi
}

# install vim files
create_link "${script_dir}/vim" ~/.vim
create_link ~/.vim/vimrc ~/.vimrc
test ! -d ~/.vim/bundle && mkdir ~/.vim/bundle
test ! -d ~/.vim/autoload && mkdir ~/.vim/autoload
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
modules="bling/vim-airline klen/python-mode scrooloose/nerdtree scrooloose/syntastic tpope/vim-fugitive fatih/vim-go"
for module in ${modules}
do
    install_or_update_vim_module "${module}"
done

# install tmux files
create_link "${script_dir}/tmux/tmux.conf" ~/.tmux.conf

# install bashrc files
create_link "${script_dir}/bashrc.insomniac" ~/.bashrc.insomniac
if [ -e ~/.bashrc ]
then
    grep -q 'source ~/.bashrc.insomniac' ~/.bashrc || echo "source ~/.bashrc.insomniac" >> ~/.bashrc
else
    echo "source ~/.bashrc.insomniac" >> ~/.bashrc
fi

# install gitconfig files
create_link "${script_dir}/gitconfig/gitconfig" ~/.gitconfig

# install scripts
# install bgs's dependencies
go install github.com/google/codesearch/cmd/csearch@latest
go install github.com/google/codesearch/cmd/cindex@latest

if [ ! -d ~/bin/ ]
then
    mkdir ~/bin
elif [ -e ~/bin ] && [ ! -d ~/bin ]
then
    echo "[!] error: ~/bin already exists, and it's not a directory"
fi
cp "${script_dir}"/scripts/* ~/bin/
