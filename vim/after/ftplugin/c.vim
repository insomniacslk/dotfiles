" mostly from http://www.alexeyshmalko.com/2014/using-vim-as-c-cpp-ide/

set tabstop=8
set shiftwidth=8
set noexpandtab
set colorcolumn=80
highlight ColorColumn ctermbg=darkgray

augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END

let &path.="src/include,/usr/include/,"
set includeexpr=substitute(v:fname,'\\.','/','g')

