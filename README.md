# README #

This repository contains my vimrc, tmux, bashrc and gitconfig configurations.

## Dependencies ##

### for vim ###

* [pathogen](https://github.com/tpope/vim-pathogen)
* [vim-airline](https://github.com/bling/vim-airline)
* [python-mode](https://github.com/klen/python-mode)
* [nerdtree](https://github.com/scrooloose/nerdtree)
* [syntastic](https://github.com/scrooloose/syntastic)
* [vim-fugitive](https://github.com/tpope/vim-fugitive)

### for tmux ###

nothing else than tmux itself.

## Installation ##

### Via script

Just run `./install_or_update.sh`.

### Manually

#### vim
Check out the repository:

```bash
git clone https://insomniacslk@bitbucket.org/insomniacslk/dotfiles.git ~/dotfiles
```

Then either use this vimrc directly:

```bash
ln -s ~/dotfiles/vim ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc
```

or import it by adding these lines to your ```.vimrc```:

```
if filereadable($HOME.'/dotfiles/vim/vimrc')
  source $HOME/dotfiles/vim/vimrc
endif
```

Then download the dependencies:

```
cd ~/.vim
./update_all.sh
```

Replace the dotfiles/vim/vimrc path appropriately.

#### tmux

```bash
ln -s ~/dotfiles/tmux/tmux.conf ~/.tmux.conf
```

#### bashrc

```bash
ln -s ~/dotfiles/bashrc.insomniac ~/.bashrc.insomniac
```

then add the following to `~/.bashrc`:

```
source ~/.bashrc.insomniac
```

#### gitconfig
```bash
ln -s ~/dotfiles/gitconfig/.gitconfig ~/.gitconfig
```

### scripts

Just copy all the scripts to ~/bin . Some of them have dependencies:

* `bgs` requires `cindex` and `csearch` from https://github.com/google/codesearch
